import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  interval;
  counter = 0;
  @Output() intervalFired = new EventEmitter<number>();

  ngOnInit() {
  }

  onStartGame() {
    this.interval = setInterval(() => {
      this.intervalFired.emit(this.counter + 1);
      this.counter++;
    }, 1000);
  }

  onStopGame() {
    if (this.interval !== 'undefined') {
      clearInterval(this.interval);
      this.counter = 0;
    }
  }
}
